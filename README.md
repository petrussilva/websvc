How to run the container:

docker run -dit -p 5000:5000 docker.io/petrusgf/websvc:1.0

the command will run a public container that I've uploaded to my repo. 

The code is on websvc.py file. The program accepts methods 'GET' and 'POST':

GET: 
Full list of URI: yourip:5000/urlinfo/1
Example GET specific address: http://yourip:5000/urlinfo/1/cisco.com:80/index.html

POST:
POST accept JSON format, you have to inform Domain, URI and Result:
Address: http://yourip:5000/urlinfo/post
JSON Example:

    {
        "domain":"gmx.com:443",
        "uri":"/index.html",
        "result":"OK"
        
    }


I've just create a POST method, we could use PUT and DELETE methods to update the database.

The unittest is on websvc_test.py, it tests a GET.

The size of the URL list could grow infinitely, how might you scale this beyond the memory capacity of this VM? Bonus if you implement this.
For this problem we create a limit to 2000 characters following the HTTP standard, if reach that value we show an error 414(URL too long). On this case we should try to figure out why they are not following the standard.

The number of requests may exceed the capacity of this VM, how might you solve that? Bonus if you implement this.
For this problem we can use ELastic load balancing +AUTO Scaling based in Cloudwatch metrics.

                        +-------------------+
                        +Cloud Watch Metrics+
                        +-------------------+
                                  |                                  
                                  |
                                  V
                +--------+      +---+       +------------+
                +internet+  --> +ELB+  -->  +Auto-Scaling+
                +--------+      +---+       +------------+



If reach a metric, an example 90% of cpu usage or 80% of memmory usage, we can scale UP or scale DOWN the environment using just what we need. Cloudwatch can provide metrics for this monitoring.
(https://docs.aws.amazon.com/autoscaling/ec2/userguide/as-instance-monitoring.html)


What are some strategies you might use to update the service with new URLs? Updates may be as much as 5 thousand URLs a day with updates arriving every 10 minutes.
The local database is sqlite and can handle 5.000 updates per day, but for long term is not good deal, as we are dealing with lots of data I would recommend NOSQL instance as the data model is pretty simple,maybe Mongodb solution would be a good fit for this system, they povide perfomance and scalability.








